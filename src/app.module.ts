import { INestApplication, Module } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { controllers } from './setup.controllers';
import { useCases } from './setup.usecases';

@Module({
  imports: [],
  controllers: [...controllers],
  providers: [...useCases],
})
export class AppModule {
  static async createDocument(app: INestApplication) {
    const options = new DocumentBuilder()
      .setTitle('API')
      .setDescription('API description')
      .setVersion('1.0')
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('docs', app, document);
  }
}
